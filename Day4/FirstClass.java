package day4assignment;

public class FirstClass {
	int n = 100;
	public FirstClass() {
		System.out.println("in the constructor of class FirstClass: ");
		System.out.println("n = " + n);
		n = 333;
		System.out.println("n = " + n);
	}
	public void setFirstClass(int value) {
		n = value;
	}
	public int getFirstClass() {
		return n;
	}
} // class FirstClass

