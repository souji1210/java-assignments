package day4assignment;

public class Square extends Shape {
private Integer s;

public Integer getSide() {
	return s;
}

public void setSide(Integer s) {
	this.s = s;
}
public float calculatedArea() {
	Float area=Float.valueOf((float)s*s);
	return area;
	}

public Square(String n, Integer s) {
	super(n);
	this.s = s;
}

}