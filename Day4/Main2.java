package day4assignment;

import java.util.Scanner;

import day4assignment.MemberShipCard;
import day4assignment.PaybackCard;

public class Main2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		System.out.println("Select the Card :");
		System.out.println("1.Membership Card");
		System.out.println("2.Payback Card :");
		System.out.println("Select the option");
		int input = in.nextInt();
		System.out.println("Enter the Card Details : ");
		String details = in.next();
		String[] w = details.split("[|]");
		switch (input) {
		case 1:
			System.out.println("Enter the Rating");
			int r1 = in.nextInt();
			MemberShipCard c= new MemberShipCard(w[0], w[1], w[2], r1);
			System.out.println(c.getHolderName() + "'s Mambership Card Details");
			System.out.println("Card Number : " + c.getCardNumber());
			System.out.println("Expiry Date : " + c.getExpiryDate());
			System.out.println("Rating : " + c.getRating());
			break;
		case 2:
			System.out.println("Enter Points in the Card");
			int p = in.nextInt();
			System.out.println("Enter the Amount");
			double a = in.nextDouble();
			PaybackCard c1 = new PaybackCard(w[0], w[1], w[2], p, a);
			System.out.println(c1.getHolderName() + "'s Payback Card Details");
			System.out.println("Card Number : " + c1.getCardNumber());
			System.out.println("Expiry Date : " + c1.getExpiryDate());
			System.out.println("Points Earned : " + c1.getPointsEarned());
			System.out.println("Total Amount : " + c1.getTotalAmount());
			break;
		default:
			System.out.println("Select the Vaild Card");
			break;
		}
		in.close();
	}

}
