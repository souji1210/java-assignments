package day4assignment;

public class MemberShipCard extends Card {
	private int r;

	public MemberShipCard(String holderName, String cardNumber, String expiryDate, int rating) {
		super(holderName, cardNumber, expiryDate);
		this.r = rating;
	}

	public int getRating() {
		return r;
	}

	public void setRating(int rating) {
		this.r = rating;
	}
}
