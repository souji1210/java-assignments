package day4assignment;


public class Rectangle extends Shape {
	private Integer l;
	private Integer b;
	public Integer getLength() {
		return l;
	}
	public void setLength(Integer l) {
		this.l = l;
	}
	public Integer getBreadth() {
		return b;
	}
	public void setBreadth(Integer b) {
		this.b = b;
	}
	public Rectangle(String n, Integer l, Integer b) {
		super(n);
		this.l = l;
		this.b = b;
	}
	public float calculatedArea() {
		Float a=Float.valueOf((float)l*b);
		return a;
	}

}
