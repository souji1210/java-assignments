package day4assignment;

public class SecondClass {
		double n1 = 123.45;
		public SecondClass() {
			System.out.println("-----in the constructor of class B: ");
			System.out.println("b = " + n1);
			n1 = 3.14159;
			System.out.println("b = " + n1);
		}
		public void setSecondClass(double value) {
			n1 = value;
		}
		public double getSecondClass() {
			return n1;
		}
	} // class SecondClass
