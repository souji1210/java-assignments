package day4assignment;

public abstract class Shape {
      protected String n;

	public String getName() {
		return n;
	}

	public void setName(String n) {
		this.n = n;
	}

	public Shape(String n) {
		this.n = n;
	}
      abstract float calculatedArea();
}
