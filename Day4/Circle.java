package day4assignment;

public  class Circle extends Shape {
	private Integer r;

	public Integer getRadius() {
		return r;
	}

	public void setRadius(Integer radius) {
		this.r = radius;
	}

	public float calculatedArea() {
		Float area = Float.valueOf(r * r * 3.14f);
		return area;
	}

	public Circle(String name, Integer radius) {
		super(name);
		this.r = radius;
	}

	
}
