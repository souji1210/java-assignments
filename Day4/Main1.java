
package day4assignment;

import java.util.Scanner;

import day4assignment.Circle;
import day4assignment.Rectangle;
import day4assignment.Square;

public class Main1 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter shape: ");
		System.out.println("1.Circle\n2.Rectangle\n3.Square");
		String option = in.next();
		if (option.equals("Circle")) {
			System.out.println("Enter the radius: ");
			int r = in.nextInt();
			Circle cir = new Circle("Cir", r);
			System.out.println(cir.calculatedArea());
		} else if (option.equals("Rectangle")) {
			System.out.println("Enter the length: ");
			int l = in.nextInt();
			System.out.println("Enter the breadth: ");
			int b = in.nextInt();
			Rectangle rect = new Rectangle("Rect", l, b);
			System.out.println(rect.calculatedArea());

		} else if (option.equals("Square")) {
			System.out.println("Enter the side: ");
			int s = in.nextInt();
			Square squ = new Square("Squ", s);
			System.out.println(squ.calculatedArea());
		} else {
			System.out.println("Not matched");
		}

		in.close();
	}

}