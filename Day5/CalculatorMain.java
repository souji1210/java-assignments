package day5;
import java.util.Scanner;
class CalculatorMain {

	public static void main(String[] argh) {
		Scanner scanner = new Scanner(System.in);

		while (scanner.hasNextInt()) {
			int n = scanner.nextInt();
			int p = scanner.nextInt();
			Calculator calculator = new Calculator();
			try {
				System.out.println(calculator.power(n, p));
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		scanner.close();

	}
}