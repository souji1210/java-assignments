package day5;
import java.util.Scanner;
public class CustomException {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws InvalidAgeException {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter player name: ");
		String nameofplayer = in.next();
		System.out.println("Enter player age: ");
		int ageofplayer = in.nextInt();
		try {
			if (ageofplayer <= 18) 
			{
				throw new InvalidAgeException();
			}
			else 
			{
				System.out.println("Player name: " + nameofplayer);
				System.out.println("Player age: " + ageofplayer);
			}
		    }
	            	catch (InvalidAgeException iae) {
			        System.out.println("CustomException: InvalidAgeRangeException ");
		}

		in.close();
	}
}
