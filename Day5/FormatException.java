package day5;
import java.util.Scanner;

public class FormatException {
	Scanner in=new Scanner(System.in);
    int r, b;
    float runRate;
    public void input(){
        try{
            System.out.println("Enter the total runs scored: ");
            r=in.nextInt();
            System.out.println("Enter the total overs faced: ");
            b=in.nextInt();
        }
        catch(NumberFormatException e){
            System.out.println("Error Code: "+e);
            System.exit(0);
        }
    }

    public void compute(){
        runRate=r/b;
        System.out.println("Score is "+r+" runs in "+b+" balls with the Run Rate of "+runRate+" runs per over.");
    }
}
