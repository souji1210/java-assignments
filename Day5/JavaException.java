package day5;
import java.util.*;

public class JavaException {

	public static void main(String[] args) {
		System.out.println("enter 2 numbers: ");
		Scanner in = new Scanner(System.in);

		try {
			
			try {
				int n1 = new Integer(in.nextInt());
				int n2 = new Integer(in.nextInt());
				System.out.println("Dividing Numbers: " + (n1 / n2));
			} catch (InputMismatchException e) {
				System.out.println("java.util.InputMismatchException");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		in.close();
	}
}
