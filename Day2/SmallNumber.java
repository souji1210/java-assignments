package day2;

public class SmallNumber {
	public void printSmallest(int n1, int n2, int n3) {

		if (n1 <= n2 && n1 <= n3) {
			System.out.println(n1 + " is the smallest number");
		} else if (n2 <= n1 && n2 <= n3) {
			System.out.println(n2 + " is the smallest number");
		} else {
			System.out.println(n3 + " is the smallest number");

		}

	}

}
