package day2;

import java.util.Scanner;

class A {
	public int add(int n1, int n2) {
		return n1 + n2;
	}

	public int sub(int n1, int n2) {
		return n1 - n2;
	}

}

 class B extends A {
	public int mul(int n1, int n2) {
		return n2 * n2;
	}

}

public class InheritanceExample {
	public static void main(String[] args) {
				System.out.println("Enter Numbers: ");
				Scanner in = new Scanner(System.in);
				int n1 = in.nextInt();
				int n2 = in.nextInt();
					B b = new B();
				System.out.println("Addition: " + b.add(n1, n2));
				System.out.println("Subtraction: " + b.sub(n1, n2));
				System.out.println("Multiplication: " + b.mul(n1, n2));
				in.close();

			}

		}