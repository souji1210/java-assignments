package day2;
import java.util.Scanner;
public class AdditionMain {

	public static void main(String[] args) {
		Addition add1 = new Addition();
		Scanner in = new Scanner(System.in);
		System.out.println("First number");
		int num1 = in.nextInt();
		System.out.println("Second number");
		int num2 = in.nextInt();
		System.out.println("Third number");
		int num3 = in.nextInt();
		System.out.println("Fourth number");
		int num4 = in.nextInt();
		System.out.println("Fifth number");
		int num5 = in.nextInt();
		System.out.println("Sixth number");
		int num6 = in.nextInt();
		System.out.println(num1 + "+" + num2 + "=" + add1.add(num1, num2));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "=" + add1.add(num1, num2, num3));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "+" + num4 + "=" + add1.add(num1, num2, num3, num4));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "+" + num4 + "+" + num5 + "="
				+ add1.add(num1, num2, num3, num4, num5));
		System.out.println(num1 + "+" + num2 + "+" + num3 + "+" + num4 + "+" + num5 + "+" + num6 + "="
				+ add1.add(num1, num2, num3, num4, num5, num6));

	}

}
