package day2;

import java.util.Scanner;

public class Vowels {

	public static void main(String[] args) {
		System.out.print("Enter string: ");
		Scanner in = new Scanner(System.in);
		String s1 = in.nextLine();
		System.out.print("No of Vowels: " + countVowels(s1) + "\n");
		in.close();
	}

	public static int countVowels(String s) {
		int c= 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == 'a' || s.charAt(i) == 'e' || s.charAt(i) == 'i' || s.charAt(i) == 'o'
					|| s.charAt(i) == 'u' || s.charAt(i) == 'A' || s.charAt(i) == 'E'
					|| s.charAt(i) == 'I' || s.charAt(i) == 'O' || s.charAt(i) == 'U') {
				c++;
			}
		}
		return c;
	}
}
