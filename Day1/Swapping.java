package day1assignment;
import java.util.*;  

public class Swapping {
	public static void main(String[] args) {  
	       int a, b, temp;// x and y are to swap   
	       Scanner in = new Scanner(System.in);  
	       System.out.println("Enter the value of a and b: ");  
	       a =in.nextInt();  
	       b =in.nextInt();  
	       System.out.println("before swapping numbers: "+a+"  "+ b);  
	       /*swapping */  
	       temp = a;  
	       a = b;  
	       b = temp;  
	       System.out.println("After swapping: "+a +"   " + b);  
	       System.out.println( );  
	    }    

}

