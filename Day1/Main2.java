package day1;
import java.util.Scanner;
class SumOfSquare {
	public static int sumOfSquaresOfEvenDigits(int n1) {
		int s = 0, r;
		while (n1 > 0) {
			r = n1 % 10;
			if (n1 % 2 == 0)
				s = s + (r * r);
			n1 = n1 / 10;
		}
		return s;
	}

}

public class Main2 {
	public static void main(String[] args) {
		System.out.println("Enter the number: ");
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a = SumOfSquare.sumOfSquaresOfEvenDigits(n);
		System.out.println("Sum of squares of even digit is: " + a);
		in.close();
	}
}


