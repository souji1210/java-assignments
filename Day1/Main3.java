package day1;
import java.util.Scanner;
class LargestWord {
	public static String getLargestWord(String str) {
		String[] str1 = str.split(" ");
		int maximum = 0;
		String r = "";
		for (int i = 0; i < str1.length; i++) {
			int len = str1[i].length();
			if (maximum < len) {
				maximum = len;
				r = str1[i];
			}
		}
		return r;
	}
}
public class Main3 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the String:");
		String s = in.nextLine();
		System.out.println(LargestWord.getLargestWord(s));
		in.close();
	}

}