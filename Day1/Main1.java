package day1;

import java.util.Scanner;
class OddSum {
	public static int checkSum(int n) {
		int s=0;
		while(n>0) {
			int r=n%10;
			if(r%2!=0) 
				s=s+r;
				n=n/10;
			if(s%2==0)
				return -1;
			else
				return 1;
		}
		return s;
	}
}
public class Main1 {
public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the number");
		int number = in.nextInt();
		int r1=OddSum.checkSum(number);
		if(r1==-1)
			System.out.println("Sum of odd digits is Even");
		else
			System.out.println("Sum of odd digits is Odd");
		in.close();
	}
}