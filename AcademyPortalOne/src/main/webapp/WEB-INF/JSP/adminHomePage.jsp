<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin HomePage</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/popup.css">
</head>
<body>
<div class="topnav">
  <a class="active" href="#">Academy Portal</a>
  <a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
  <div class="topnav-right">
				<a href="logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
			</div>
			
</div>
<br><br><br><br>
<center><h1 class="heading2">WELCOME TO ADMIN PAGE</h1></center>
<div align="center">
<table>
<tr>
<td><i class="fa fa-edit" style="font-size:30px;color:#3C4551"></i></td>
<td><a class="acss" style="cursor:pointer"
  onclick=" window.open('module','',' scrollbars=yes,menubar=no,width=700, resizable=yes,toolbar=no,location=no,status=no')"><p>Module Registration</p></a></td>
</tr>

<tr>
<td><i class="fa fa-edit" style="font-size:30px;color:#3C4551"></i></td>
<td><a class="acss" style="cursor:pointer"
  onclick=" window.open('batch','',' scrollbars=yes,menubar=no,width=500, resizable=yes,toolbar=no,location=no,status=no')"><p>Batch Allocation</p></a></td>
</tr>

<tr>
<td><i class="fa fa-edit" style="font-size:30px;color:#3C4551"></i></td>
<td><a class="acss" style="cursor:pointer"
  onclick=" window.open('batchUpdate','',' scrollbars=yes,menubar=no,width=500, resizable=yes,toolbar=no,location=no,status=no')"><p>Batch Updation</p></a></td>
</tr>


<tr>
<td><i class="fa fa-edit" style="font-size:30px;color:#3C4551"></i></td>
<td><a class="acss" style="cursor:pointer"
  onclick=" window.open('report','',' scrollbars=yes,menubar=no,width=500, resizable=yes,toolbar=no,location=no,status=no')"><p>Report Management</p></a></td>
</tr>


<tr>
<td><i class="fa fa-edit" style="font-size:30px;color:#3C4551"></i></td>
<td><a class="acss" style="cursor:pointer"
  onclick=" window.open('facultyView','',' scrollbars=yes,menubar=no,width=500, resizable=yes,toolbar=no,location=no,status=no')"><p>Faculty Details</p></a></td>
</tr>

</table>
</div>




</div>





<!-- <tr><th><i class="fa fa-edit" style="font-size:36px;color:#3C4551"></i></th><td><a href="facultyView"><b>Faculty Details</b></a><br/></td></tr><br/>
<tr><th><i class="fa fa-edit" style="font-size:36px;color:#3C4551"></i></th><td><a href="module" target="_blank"><b>Module Registration</b></a><br/></a><br></td></tr><br/>
<tr><th><i class="fa fa-edit" style="font-size:36px;color:#3C4551"></i></th><td><a href="batch" target="_blank"><b>Batch Allocation</b></a><br/></td></tr><br/>
<tr><th><i class="fa fa-edit" style="font-size:36px;color:#3C4551"></i></th><td><a href="batchUpdate" target="_blank"><b>Batch Updation</b></a><br/></a><br></td></tr><br/>
<tr><th><i class="fa fa-edit" style="font-size:36px;color:#3C4551"></i></th><td><a href="report" target="_blank"><b>Report Management</b></a><br/></a><br></td></tr></table></center>
 -->
</body>
</html>



