<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/select.css">
<style type="text/css">
.tablepro
{border-collapse:collapse;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report Page</title>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		<div class="topnav-right">
			<a href="reportViews">Batch Details</a>
		</div>
	</div>
	<br />
	<br />
	<br />
	<br />
	<center>
<form:form action="reportSubmission" modelAttribute="batch" style="max-width: 450px; margin:0 auto;">
<p class="heading2">Select BatchId To Generate Report</p>
<p class="heading2">Batch Id</p>
<form:select path="batchId" cssClass="select">
<%-- <form:option value="" label="Select BatchId" cssClass="option"/> --%>
<c:forEach var="id" items="${batchId}">
<form:option value="${id.batchId}" cssClass="option"></form:option>
</c:forEach>
</form:select><br/>
<input type="submit" value="submit" class="btn btn-primary">
</form:form></center>
</body>
</html>
