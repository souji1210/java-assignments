package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="LOGIN_TABLE")
public class Login {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Id;
	@NotEmpty(message="Name should not be empty")
	private String userName;
	@NotEmpty(message="Password should not be empty")
	private String password;
	public Login() {
		super();
	}
	public Login(int userId, String userName, String password) {
		super();
		this.Id = userId;
		this.userName = userName;
		this.password = password;
	}
	public int getId() {
		return Id;
	}
	public void setId(int Id) {
		this.Id = Id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
