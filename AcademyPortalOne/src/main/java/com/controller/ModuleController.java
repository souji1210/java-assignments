package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.ModuleRegistration;
import com.service.ModuleRegistrationServiceIntf;
@Controller
public class ModuleController {
	@Autowired
	ModuleRegistrationServiceIntf service;
	
	@RequestMapping("/module")
	public ModelAndView moduleRegistration(@ModelAttribute("moduleregistration") ModuleRegistration moduleregistration) {
		return new ModelAndView("moduleRegistration");// jsp
	}
	
	@RequestMapping("/saveModuleRegistration")
	public ModelAndView saveModuleRegistration(@Validated @ModelAttribute("moduleregistration") ModuleRegistration moduleregistration, BindingResult result) {
		if (result.hasErrors()) {
			return new ModelAndView("moduleRegistration");
		} else {
			service.saveModuleRegistration(moduleregistration);
			return new ModelAndView("redirect:/viewDetails");
			// return new ModelAndView("success");
		}
	}
	
	@RequestMapping("/moduleUpdate")
	public ModelAndView ModuleUpdate(@ModelAttribute("moduleregistration") ModuleRegistration moduleregistration) {
		List<ModuleRegistration> ls = service.viewDetails();
		return new ModelAndView("moduleUpdation", "listOfUsers", ls);
		
	}
	
	@RequestMapping(value="/updateModule/{id}")    
    public ModelAndView edit(@PathVariable int id,ModuleRegistration moduleregistration,Model model){
        moduleregistration=service.getDetails(id);
        model.addAttribute("command",moduleregistration);
        return new ModelAndView("moduleEdit");    
    }    
    
    
    @RequestMapping("/moduleSave")    
    public ModelAndView editSave(ModuleRegistration moduleregistration){
        service.edit(moduleregistration);
        return new ModelAndView("redirect:/moduleUpdate");    
    }   
    
    @RequestMapping(value="/deleteModule/{id}")    
    public ModelAndView delete(@PathVariable int id){
        service.delete(id);
        return new ModelAndView("redirect:/moduleUpdate");    
    }  

	

	@RequestMapping("/viewDetails")

	public ModelAndView viewDetails() {
		List<ModuleRegistration > ls = service.viewDetails();
		return new ModelAndView("viewModuleRegistration", "listOfUsers", ls);
	}

}


