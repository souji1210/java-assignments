package com.service;

import java.util.List;

import com.model.Batch;
import com.model.ModuleRegistration;

public interface BatchServiceIntf {
	public void saveBatch(Batch batch);
	public List<Batch> getUsers();
	public Batch getDetails(int id);
	public void edit(Batch batch);
	public void delete(int id);
	public List<ModuleRegistration> getCourse();
}
