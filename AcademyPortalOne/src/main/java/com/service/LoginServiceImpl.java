package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.LoginDao;
import com.model.Login;
@Service
public class LoginServiceImpl implements LoginService{
	@Autowired
	LoginDao dao;
	public int checkUser(Login login) {
		int result=dao.usersCheck(login);
		return result;
	}

}
