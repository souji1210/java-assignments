package com.service;

import java.util.List;

import com.model.ModuleRegistration;

public interface ModuleRegistrationServiceIntf {
	void saveModuleRegistration(ModuleRegistration moduleregistration);

	List<ModuleRegistration> viewDetails();
	public ModuleRegistration getDetails(int id);
	public void edit(ModuleRegistration moduleregistration);
	public void delete(int id);
}
