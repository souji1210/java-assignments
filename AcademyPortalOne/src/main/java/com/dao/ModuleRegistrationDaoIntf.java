package com.dao;

import java.util.List;

import com.model.ModuleRegistration;

public interface ModuleRegistrationDaoIntf {
	void saveBatch(ModuleRegistration moduleregistration);

	List<ModuleRegistration> viewDetails();
	
	ModuleRegistration getDetails(int id);

	void edit(ModuleRegistration moduleregistration);

	void delete(int id);
}
