package com.dao;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Batch;
import com.model.Registration;

@Repository
public class RegisterDaoImpl implements RegisterDaoIntf {
	@Autowired
	SessionFactory sessionFactory;
	public void saveDetails(Registration registration) {
		sessionFactory.openSession().save(registration);
	}
	
	public int checkUsers(Registration registration) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from faculty_tabl where firstName=:id and password=:password").addEntity(Registration.class);
		query.setParameter("id",registration.getFirstName());
		query.setParameter("password",registration.getPassword());
		registration=(Registration) query.uniqueResult();
		if(query.uniqueResult()==null) {
			return 0;
		}else {
			registration=(Registration) query.uniqueResult();
			return registration.getAssociateId();
		}
		
		
	}

	public List<Registration> getFacultyDetails() {
		List<Registration> ls=sessionFactory.openSession().createQuery("from Registration").list();
		return ls;
	}

	public void delete(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("delete from faculty_tabl where associateId=:id").addEntity(Registration.class);
		query.setParameter("id",id);
		query.executeUpdate();
		
	}

	
	public void edit(Registration register) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update faculty_tabl set firstName=:name where associateId=:id").addEntity(Registration.class);
		query.setParameter("name",register.getFirstName());
		query.setParameter("id",register.getAssociateId());
		query.executeUpdate();
	}
	

	public Registration getDetails(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from faculty_tabl where associateId=:id").addEntity(Registration.class);
		query.setParameter("id",id);
		Registration register=(Registration) query.uniqueResult();
		return register;
	}

	public List<Batch> getbfDetails(int faculty_id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from batch_tabl where register_associateId=:id").addEntity(Batch.class);
		query.setParameter("id",faculty_id);
		List<Batch> list=query.list();
		return list;
	}

	
	
	

}
