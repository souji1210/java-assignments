package day6;

import java.util.Scanner;
public class NumberValidation {
  public static void main(String[] args) {
	  System.out.println("enter the phone number in valid form: ");
    Scanner in = new Scanner(System.in);
     String str = in.nextLine();
    
    if(str.matches("[0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4}")){
      System.out.println("Valid number format");
    }else{
      System.out.println("Invalid number format");
      in.close();
    }
  }
}