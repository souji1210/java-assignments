package day6;
import java.util.Scanner;
public class SumOfOdd {
  
  public static void main(String[] args) {
	  System.out.println("Enter the number:");
    Scanner in = new Scanner(System.in);
    int number = in.nextInt();
    int sum=0;
    while(number>0){
      int rem = number%10;
      if(rem%2!=0){
        sum = sum+rem;
      }
      number = number/10;
    }
    
    if(sum%2==0){
      System.out.println("Sum of odd digits is even");
    }else{
      System.out.println("Sum of odd digits is odd");
    }
    
  }
}