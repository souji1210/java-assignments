package day6;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.StringTokenizer;
public class User {
	public static String convertDateFormat(String s) throws ParseException{
		
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		Date d=sdf.parse(s);
		SimpleDateFormat sdf1=new SimpleDateFormat("dd-MM-yyyy");
		String sum=sdf1.format(d);
		return sum;
		
	}

}
