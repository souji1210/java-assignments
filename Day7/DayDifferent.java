package day6;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Calendar;
import java.io.InputStreamReader;
import java.text.*;
import java.util.*;

public class DayDifferent {
	
      public static void main(String[] args) throws ParseException, IOException {
      BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
      
      String str1 = b.readLine();
      String str2= b.readLine();
      System.out.println(monthsBetweenDates(str1,str2));
      }
      public static int monthsBetweenDates(String s1, String s2) throws ParseException {
    		
    	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
    	    Date d1=sdf.parse(s1);
    	    Date d2=sdf.parse(s2);
    	    Calendar c=Calendar.getInstance();
    	    c.setTime(d1);
    	    int m1=c.get(Calendar.MONTH);
    	    int y1=c.get(Calendar.YEAR);
    	    c.setTime(d2);
    	    int m2=c.get(Calendar.MONTH);
    	    int y2=c.get(Calendar.YEAR);
    	    int n=((y2-y1)*12)+(m2-m1);
    	    return n;
    	}
   }

