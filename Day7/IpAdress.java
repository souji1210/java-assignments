package day6;
import java.util.*;
public class IpAdress {
  public static void main(String[] args) {
	  System.out.println(" Enter the Ip address: ");
    Scanner in = new Scanner(System.in);
    
    String ipAdr = in.nextLine();
    boolean b = validateIpAddress(ipAdr);
    if (b == true)
      System.out.println("Valid Ip Address");
    else
      System.out.println("Invalid Ip Address");
  }
  public static boolean validateIpAddress(String ipAdr) {
    boolean b1 = false;
    StringTokenizer t = new StringTokenizer(ipAdr, ".");
    int a = Integer.parseInt(t.nextToken());
    int b = Integer.parseInt(t.nextToken());
    int c = Integer.parseInt(t.nextToken());
    int d = Integer.parseInt(t.nextToken());
    if ((a >= 0 && a <= 255) && (b >= 0 && b <= 255)
        && (c >= 0 && c <= 255) && (d >= 0 && d <= 255))
      b1 = true;
    return b1;
   
  }
}
