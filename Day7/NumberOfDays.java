package day6;
import java.util.Calendar;
import java.util.*;
public class NumberOfDays {
  public static void main(String[] args) {
	  System.out.println("Enter the Year and month : ");
    Scanner in = new Scanner(System.in);
    int year = in.nextInt();
    int month = in.nextInt();
    System.out.println(display(year, month));
  }
  public static int display(int year, int month) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, year);
    calendar.set(Calendar.MONTH, month);
    int DAY_OF_MONTH = calendar.getActualMaximum(calendar.DAY_OF_MONTH);
    return DAY_OF_MONTH;
  }
}