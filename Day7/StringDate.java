package day6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
public class StringDate {
   
    public static void main(String[] args) 
    {
        String date = new String();
        Scanner in =  new Scanner(System.in);
        System.out.println("Enter a String date in yyyy-mm-dd format only");
        date=in.nextLine();
      try 
      {    
          //Format for string is specified here and passed as a parameter in the constructor
      Date format = new SimpleDateFormat("yyyy-MM-dd").parse(date);
      //Two methods viz parse and format are used to extract Date from the string
      String date1 = new SimpleDateFormat("dd/MM/yyyy").format(format);
       System.out.println("The date is "+date1+" (dd/mm/yyyy) ");
      } 
    catch (Exception e) {
        System.out.println("Exception occured "+e);
     }
        
        
    }
    
}