package day6;
import java.util.*;

public class StringFristLast {

     static int checkCharacter(String sentance)
    {
    	 sentance=sentance.toLowerCase();
        if (sentance.charAt(0) == sentance.charAt(sentance.length()-1)) 
           return 1;
        else
           return -1;
    }
    public static void main(String[] args)
    {

        System.out.println("Enter the String: ");
        Scanner in = new Scanner(System.in);
         String sentance=in.nextLine();
        int res = StringFristLast.checkCharacter(sentance);
        if (res == 1)
            System.out.println("valid Input");
        else 
            System.out.println("Invalid");
        in.close();
      
    }
}