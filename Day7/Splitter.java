package day7;
import java.util.Scanner;
public class Splitter {
	public static void main(String[] args) {
		System.out.println("enter string");//AS in Sample Input
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		System.out.println("enter Delimiter");
		char c = sc.next().charAt(0);
		String[] Split1 = str.split("/");
		for (int i = 0; i < Split1.length; i++) {
			System.out.println(Split1[i]);
		}
		System.out.println("");

		String reverse = new StringBuffer(str).reverse().toString();
		String lower = reverse.toLowerCase();
		String[] Split2 = lower.split("/");

		for (int i = 0; i < Split2.length; i++) {
			System.out.println(Split2[i]);
		}

	}


}
