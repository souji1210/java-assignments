package day6;
import java.util.Scanner;
class MiddleChar {
    public static String getMiddleCharacter(String s)
    {
                StringBuffer sb=new StringBuffer();
                 if(s.length()%2==0)
                 {
                 sb.append(s.substring((s.length()/2)-1,(s.length()/2)+1));
                 }
                 return sb.toString();
                 }
 }

public class Middle
{
  public static void main(String[] args) {
	  System.out.print("Enter the String name: ");
    Scanner in= new Scanner(System.in);
    String s1 = in.nextLine();
    String s2 = MiddleChar.getMiddleCharacter(s1);
    System.out.println(s2);
    in.close();
  }
}