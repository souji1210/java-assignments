package com.main;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.pojo.Car;
import com.service.CarServices;

public class CarMain {

	public static void main(String[] args) throws SQLException{
		String make,model;
		int year;
		float sales_price;
		List<Car> carlist;
		List<Car> list=new ArrayList<Car>();
		System.out.println("Welcome to mullet joes gently used autos! ");
		Scanner scanner=new Scanner(System.in);
		CarServices carServices=new CarServices();
		Car car=new Car();
		while(true) {
			System.out.println("1: add");
			System.out.println("2: list ");
			System.out.println("3: delete");
			System.out.println("3: update");
			System.out.println("4: quit ");
			System.out.println("Enter your choice");
			String choice=scanner.next();
			if(choice.equalsIgnoreCase("quit"))
			{
				System.out.println("Good BYE!!");
				break;
			}
			else {
				switch(choice) {
				case"add":
					System.out.println("Enter the Make.");
					make=scanner.next();
					System.out.println("Enter the Model.");
					model=scanner.next();
					System.out.println("Enter the year.");
					year=scanner.nextInt();
					System.out.println("enter sales price");
					sales_price=scanner.nextFloat();
					car.setMake(make);
					car.setModel(model);
					car.setYear(year);
					car.setSales_price(sales_price);
					try
					{
						carServices.addCar(car);
					}
					catch(SQLException e) {
						e.printStackTrace();
					}
					break;
				case"list":
					System.out.println("Displays all list");
					carServices.viewCars();
					
					break;
				case"delete":
					System.out.println("Enter Make to delete");
					make=scanner.next();
					System.out.println("Enter Model to delete");
					model=scanner.next();
					String status;
					try
					{
						status=carServices.deleteCar(make,model);
						System.out.println(status);
					}
					catch(SQLException e) {
						e.printStackTrace();
					}
					break;
					
				case"update":
					System.out.println("enter sales_price to be updated");
					sales_price=scanner.nextFloat();
					System.out.println("Enter Make.");
					make=scanner.next();
					System.out.println("Enter Model");
					model=scanner.next();
					String status1;
					try
					{
						status1=carServices.updateCar(make,model, sales_price);
						System.out.println(status1);
					}
					catch(SQLException e) {
						e.printStackTrace();
					}
					break;
					default:
						System.out.println("Sorry,Please select valid option");
					}
				}
			}
					}

	}

