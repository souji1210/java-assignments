package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.connection.DBConnection;
import com.pojo.Car;

public class Dao{
	Connection connection;
	List<Car> list=new ArrayList<Car>();
	Car c=new Car();
	public List<Car> view() {
		ResultSet result=null;
		Statement statement=null;
		connection=DBConnection.getConnection();
		try {
			statement=connection.createStatement();
			String sql="select * from car";
			result=statement.executeQuery(sql);
			
			while(result.next()) {
				String make=result.getString(1);
				String model=result.getString(2);
				int year=result.getInt(3);
				float salesPrice=result.getFloat(4);
				c.setMake(make);
				c.setModel(model);
				c.setYear(year);
				c.setSales_price(salesPrice);
				list.add(c);
				System.out.println(c);
				
			}
		
			System.out.println("No of cars:"+count());
			System.out.println("Total Price"+average());
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			try {
				if(result!=null)
					result.close();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
			try {
				if(statement!=null)
					statement.close();
			}
			catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	public void closePreparedStmt(PreparedStatement statement) {
		try {
			if(statement!=null) {
				statement.close();
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	public Car insertCar(Car c) throws SQLException{
		connection=DBConnection.getConnection();
		int count=count();
		if(count<=20) {
		
		String sql="insert into car values(?,?,?,?)";
		PreparedStatement statement=connection.prepareStatement(sql);
		
	
		try
		{
			statement.setString(1, c.getMake());
			statement.setString(2, c.getModel());
			statement.setInt(3, c.getYear());
			statement.setFloat(4, c.getSales_price());
			statement.executeUpdate();
		}
		catch(SQLException e) {
			e.getMessage();
		}
		finally {
			closePreparedStmt(statement);
		}}
		else {
			System.out.println("Sorry!! Limit exceeded");
		}
	
		return c;
	}
	public String deleteCar(String make,String model) throws SQLException{
		connection=DBConnection.getConnection();
		String sql="delete from car where make=?and model=?";
		PreparedStatement statement=connection.prepareStatement(sql);
		try
		{
			statement.setString(1, make);
			statement.setString(2, model);
			statement.executeUpdate();
		}
		catch(SQLException se) {
			se.printStackTrace();
		}
		finally {
			closePreparedStmt(statement);
		}
		return "deleted Successfully";
	}
	//Method to update details on the car table 
	public String updateCar(String make,String model,float price)throws SQLException{
		connection=DBConnection.getConnection();
		String sql="update car set sales_price=?"+"where make=?and model=?";
		PreparedStatement statement=connection.prepareStatement(sql);
		try
		{
			statement.setFloat(1, price);
			statement.setString(2, make);
			statement.setString(3, model);
			statement.executeUpdate();
		}
		catch(SQLException se) {
			se.printStackTrace();
		}
		finally {
			closePreparedStmt(statement);
		}
		return "Updated Successfully";
	}
	public float average() throws SQLException{
		connection=DBConnection.getConnection();
		ResultSet rs=null;
		float sum=0;
		String sql="select sum(sales_price) from car";
		PreparedStatement statement=connection.prepareStatement(sql);
		try
		{
			rs=statement.executeQuery();
			while(rs.next()) {
				float c=rs.getFloat(1);
				sum=sum+c;
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			closePreparedStmt(statement);
		}
		return sum;
	}

public int count() throws SQLException {
	connection = DBConnection.getConnection();
    ResultSet rs=null;int count=0;
    String sql = "select count(*) from car";
    PreparedStatement statement = connection.prepareStatement(sql);
    try {
    rs=statement.executeQuery();
    while(rs.next()) {
    	 count=rs.getInt(1);
    }
    }catch(SQLException e) {
        e.printStackTrace();
    }finally {
        closePreparedStmt(statement);
    }
    return count;
    
}
}
