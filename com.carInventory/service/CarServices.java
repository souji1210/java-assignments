package com.service;

import java.sql.SQLException;
import java.util.List;

import com.dao.Dao;
import com.dao.Dao;
import com.pojo.Car;

public class CarServices {
 Dao dao=new Dao();
  public List<Car> viewCars() {
	  return dao.view();
  }
  public Car addCar(Car car) throws SQLException {
	  return dao.insertCar(car);
  }
  public String updateCar(String make,String model,float price) throws SQLException{
	  return dao.updateCar(make,model, price);
  }
public String deleteCar(String make,String model) throws SQLException {
	return dao.deleteCar(make,model);
	
}
}