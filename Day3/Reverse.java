package day3;
import java.util.Scanner;
class UserMainCode{
	public static String string(char c, String string) {
		String modify = " ";
		char[] c1 = string.toCharArray();
		for (int i = c1.length - 1; i >= 0; i--) {
			if (i == 0) 
				modify += c1[0];
			else 
				modify += c1[i] + Character.toString(c);
		}
		return modify;
	}
}

public class Reverse{

	public static void main(String[] args) {
		System.out.println("Enter String: ");
		Scanner in = new Scanner(System.in);
		String string = in.nextLine();
		System.out.println("Enter Character: ");
		char c2 = in.next().charAt(0);
		System.out.println("Modified string : " + UserMainCode.string(c2,string));
		in.close();

	}

}