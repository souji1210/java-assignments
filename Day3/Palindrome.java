package day3;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter string: ");
		String s = in.nextLine();
		String reverseValue = "";
		int stringLength = s.length();
		for (int i = stringLength - 1; i >= 0; i--) {
			reverseValue = reverseValue + s.charAt(i);
		}
		if (reverseValue.equals(s)) 
			System.out.println("palindrome");
		 else 
			System.out.println("Not palindrome");
		
	
	}

}
