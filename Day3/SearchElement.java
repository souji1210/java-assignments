package day3;
import java.util.Scanner;
public class SearchElement {
	public int searchElement(int[] array, int n) {  //number for search
		int number = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == (n)) {
				number = array[i];

			}
		}
		if (number == (n)) 
			System.out.println(number + "  present");
		else 
			System.out.println("Not Matching");
		
		return number;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter number of elements of array:");
		int n1 = scanner.nextInt();
		int[] array = new int[n1];
		System.out.println("Enter elements:");
		for (int i = 0; i < n1; i++) {
			array[i] = scanner.nextInt();
		}
		System.out.println("Enter search value: ");
		int n2 = scanner.nextInt();
		SearchElement search = new SearchElement();
		search.searchElement(array, n2);
		

	}

}



