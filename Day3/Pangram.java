package day3;

import java.util.Scanner;

public class Pangram {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter value of String: ");
		String s = in.nextLine();
		boolean[] alphabetList = new boolean[26];
		int indexno = 0;
		int f = 1;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
				indexno = s.charAt(i) - 'A';
			} else if (s.charAt(i) >= 'a' && s.charAt(i) <= 'z') {
				indexno = s.charAt(i) - 'a';
			}
			alphabetList[indexno] = true;
		}
		for (int i = 0; i <= 25; i++) {
			if (alphabetList[i] == false)
				f = 0;
		}
		System.out.print("String: " + s);
		if (f == 1) {
			System.out.print("String is a pangram.");
		}
		else {
			System.out.print("String is not a pangram.");
		}
		in.close();
	}

}
