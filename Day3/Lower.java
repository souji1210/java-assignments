package day3;
import java.util.Scanner;
public class Lower {

	public static void main(String[] args) {

		System.out.println("Enter the String: ");
		Scanner in = new Scanner(System.in);
		String s1 = in.nextLine();
		String lower = s1.toLowerCase();
		System.out.println("Lower case string: " + lower);
		in.close();

	}
}
