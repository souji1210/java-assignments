package day3;
import java.util.Scanner;
public class Sorting {
	public static void main(String[] args) {
		int n, t;
		Scanner in = new Scanner(System.in);
		System.out.print("Enter number of elements of an array:");
		n = in.nextInt();
		int array[] = new int[n];
		System.out.println("Enter elements:");
		for (int i = 0; i < n; i++) {
			array[i] = in.nextInt();
		}
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (array[i] > array[j]) {
					t = array[i];
					array[i] = array[j];
					array[j] = t;
				}
			}
		}
		for (int i = 0; i < n - 1; i++) {
			System.out.print(array[i] + "\n");
		}
		System.out.print(array[n - 1]);
		in.close();
	}
}
