package day3;
import java.util.Scanner;
public class Replacingchar {
	
	public static void main(String[] args) {
		System.out.println("Enter value of String: ");
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		String r=s.replace('u', 'v');
		System.out.println("Replace u with v: "+r);
		in.close();
	}

}
