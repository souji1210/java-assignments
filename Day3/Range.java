package day3;
import java.util.Scanner;

public class Range {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the String: ");
		String s = in.nextLine();
		System.out.println("Enter begin index: ");
		int first = in.nextInt();
		System.out.println("Enter the End Index: ");
		int end = in.nextInt();
		System.out.println(s.substring(first, end));

	}

}
