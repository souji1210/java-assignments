package day7;
import java.time.LocalTime;
public class Main2{
	public void run() throws InterruptedException {
		Thread.sleep(10000);
		LocalTime localTime = LocalTime.now();
		System.out.println("After sleep current time : " + localTime);
	}

	public static void main(String[] args) {
		Main1 thread = new Main1();
		System.out.println("Current thread name : " +thread.getName());
		thread.setName("My Thread");
		LocalTime time = LocalTime.now();
		System.out.println("Before sleep current Time : " + time);
		thread.start();
		System.out.println("Changed thread Name : " + thread.getName());
	}
}

