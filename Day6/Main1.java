package day7;
import java.time.LocalTime;

public class Main1 extends Thread {
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ie) {
			System.err.println(ie.getMessage());
		}
		LocalTime localTime = LocalTime.now();
		System.out.println("After sleep current time : " + localTime);
	}

	public static void main(String[] args) {
		Main1 thread = new Main1();
		System.out.println("Current thread name : " + thread.getName());
		thread.setName("My Thread");

		LocalTime time = LocalTime.now();
		System.out.println("Before sleep current time : " + time);
		thread.start();
		System.out.println("Changed thread Name : " + thread.getName());
	}

}