package day7;
class DemoThread extends Thread{
	public void run() {
		try {
			System.out.println("Running child thread in loop : ");
			for(int k=0 ; k<10; k++) {
				Thread.sleep(2000);
				System.out.println(k);
			}
		}
			catch (InterruptedException ie) {
				System.err.println(ie.getMessage());
			}
		}
	public DemoThread() {
		
		Thread t = new Thread();
		t.start();
	}
}

public class Main4 {
	public static void main(String[] args) {
		DemoThread d1 = new DemoThread();
		d1.start();
		DemoThread d2 = new DemoThread();
		d2.start();
		DemoThread d3 = new DemoThread();
		d3.start();
		
	}
}

