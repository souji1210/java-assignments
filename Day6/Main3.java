package day7;
class DemoThread1 implements Runnable{

	public void run() {
		try {
			System.out.println("Running child thread in loop : ");
			for(int k=0 ; k<10; k++) {
				Thread.sleep(2000);
				System.out.println(k);
			}
		}
			catch (InterruptedException ie) {
				System.err.println(ie.getMessage());
			}
		}
	public DemoThread1() {
		
		
		Thread t = new Thread();
		t.start();
	}
	
}
public class Main3 {
	public static void main(String[] args) {
		DemoThread1 d1 = new DemoThread1();
		d1.run();
		DemoThread1 d2 = new DemoThread1();
		d2.run();
		DemoThread1 d3 = new DemoThread1();
		d3.run();
	}

}

