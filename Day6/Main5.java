package day7;
abstract class Number implements Runnable{
	public void run(int n) {
		System.out.println("Displaying the multiples of : "+ n);
		for (int k=1;k<=n/2;k++) {
			if(n%k==0) {
				System.out.println(k);
			}
		}System.out.println(n);
		System.out.println("Those are the multiples of : "+n);
	}
	public Number() {
		Thread t = new Thread();
		t.start();
	}

}
 class NumberImpl extends Number{
	public void run() {
		
	}
}

public class Main5 {
	public static void main(String[] args) {
		Number n1 = new NumberImpl();
		n1.run(2);
		Number n2 = new NumberImpl();
		n2.run(5);
		Number n3 = new NumberImpl();
		n3.run(8);
	}	
}