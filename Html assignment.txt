<!DOCTYPE html>
<html>
<head>
<script>
 function validateForm(){
	 var name=document.forms["Registration"]["name"].value;
	 if (name==""){
		 alert("Please enter the Name");
		 return false
	 }
	 var fname=document.forms["Registration"]["fathername"].value;
	 if (fname==""){
		 alert("Please enter the Father Name");
		 return false
	 }
	 var paddress=document.forms["student"]["postaladdress"].value;
	 if (paddress==""){
		 alert("Please enter the Postal Address ");
		 return false
	 }
	 var personalAddress=document.forms["student"]["personaladdress"].value;
	 if (personalAddress==""){
		 alert("Please enter the Personal Address ");
		 return false
	 }
	 var gender=document.forms["student"]["gender"].value;
	 if (gender==""){
		 alert("Select a gender");
		 return false
	 }
	 var city=document.forms["student"]["city"].value;
	 if (city=="Nothing"){
		 alert("Select a city");
		 return false
	 }
	 var course=document.forms["student"]["course"].value;
	 if (course=="Nothing"){
		 alert("Select a course");
		 return false
	 }
	 var state=document.forms["student"]["state"].value;
	 if (state=="Nothing"){
		 alert("Select a state");
		 return false
	 }
	 var email=document.forms["student"]["email"].value;
	 if (email==""){
		 alert("Email cannot be empty");
		 return false
	 }
	 var dob=document.forms["student"]["dob"].value;
	 if (dob==""){
		 alert("DOB can't be empty");
		 return false
	 }
	 
 }
 
</script>

<title>Registration</title>
<style>
div {
background-color: #33F2E5;
margin: 0 auto;
  width: 260px;
 }
</style>
</head>
<body>
<div>
<form name="Registration" onsubmit="return validateForm()">
<h3 align="center">Student Registration Form</h3>
<table >
<tr >
  <th width="60" align="left"></th>
  <th width="300"></th>
<tr>
 <td>Name</td>
 <td><input type="text" name="name" /></td>
</tr>
<tr>
 <td>Father Name</td>
 <td><input type="text" name="fathername"/></td>
</tr>
<tr>
 <td>Postal Address</td>
 <td><input type="text" name="postaladdress"/></td>
</tr>
<tr>
 <td>Personal Address</td>
 <td><input type="text" name="personaladdress"/></td>
</tr>
<tr>
 <td>Gender </td>
 <td><input type="radio" name="gender" value="Male"/>Male
 <input type="radio" name="gender" value="Female"/>Female
 <input type="radio" name="gender" value="Other"/>Other
 </td>
</tr>
<tr>
 <td>City</td>
 <td><select name="city">
 <option value="Nothing">select..</option>
 <option value="chennai">chennai</option>
 <option value="Hyderabad">Hyderabad</option>
 <option value="Mumbai">Mumbai</option>
 <option value="Nizamabad">Nizamabad</option>
 <option value="Chennai">Chennai</option>
 <option value="Vijayawada">Vijayawada</option>
 </select></td>
</tr>
<tr>
 <td>Course</td>
 <td><select name="course">
 <option value="Nothing">select..</option>
 <option value="C">C</option>
 <option value="C++">C++</option>
 <option value="Java">Java</option>
 <option value="Python">Python</option>
 <option value="Spring">Spring</option>
 </select></td>
</tr>
<tr>
 <td>State</td>
 <td><select name="state">
 <option value="Nothing">select..</option>
 <option value="AP">Andhra Pradesh</option>
 <option value="TG">Telangana</option>
 <option value="MH">Maharashtra</option>
 <option value="KN">Karnataka</option>
 <option value="WB">West Bengal</option>
 </select></td>
</tr>
<tr>
 <td>Pin Code</td>
 <td><input type="number" name="pincode" pattern=[0-9]{6}/></td>
</tr>
<tr>
 <td>EmailID</td>
 <td><input type="email" name="email"/></td>
</tr>
<tr>
 <td>DOB</td>
 <td><input type="date" name="dob"/></td>
</tr>
<tr>
 <td>Phone Number</td>
 <td><input type="number" name="phone" pattern=[0-9]{10}/></td>
</tr> 
<tr>
 <td><input type="reset" value="Reset"/></td>
 <td><input type="submit" value="Submit Form"/></td>
</tr>
</table>
</form>
</div>
</body>
</html>